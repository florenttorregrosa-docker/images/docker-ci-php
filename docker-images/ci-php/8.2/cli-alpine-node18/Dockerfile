FROM php:8.2-cli-alpine

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

# Even if not used because of https://github.com/moby/moby/issues/34482.
ARG COMPOSER_VERSION=2
ARG NODEJS_VERSION=18
ARG PA11Y_CI_VERSION=2

# Get Composer from official image.
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# Get NodeJS from official image.
COPY --from=node:18-alpine /usr/local /opt/node
RUN ln -s /opt/node/bin/node /usr/local/bin/node
RUN ln -s /opt/node/bin/npm /usr/local/bin/npm
RUN ln -s /opt/node/bin/npm /usr/bin/npm
RUN rm -f /usr/local/bin/yarn
RUN rm -f /usr/local/bin/yarnpkg
COPY --from=node:18-alpine /opt/yarn* /opt/yarn
RUN ln -s /opt/yarn/bin/yarn /usr/local/bin/yarn
RUN ln -s /opt/yarn/bin/yarnpkg /usr/local/bin/yarnpkg

RUN apk update \
    && apk upgrade \
    && apk add \
        $PHPIZE_DEPS \
        bash \
        curl \
        curl-dev \
        freetype-dev \
        git \
        gnupg \
        icu-dev \
        imagemagick \
        jq \
        libjpeg-turbo-dev \
        libpng-dev \
        libpq-dev \
        libwebp-dev \
        libxml2-dev \
        libzip-dev \
        mariadb-client \
        oniguruma-dev \
        openssh-client \
        patch \
        pcre-dev \
        pngquant \
        python3 \
        redis \
        rsync \
        shadow \
        sudo \
        unzip \
        wget \
        zlib-dev \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
        --with-webp \
    # The readline extension is already installed in the base image.
    && docker-php-ext-install \
        bcmath \
        curl \
        exif \
        gd \
        intl \
        mbstring \
        mysqli \
        opcache \
        pdo_mysql \
        pdo_pgsql \
        soap \
        xml \
        zip \
    && pecl install \
        apcu \
        igbinary \
        oauth \
        uploadprogress \
    && pecl install \
        --configureoptions='enable-redis-igbinary="yes"' \
        redis \
    && docker-php-ext-enable \
        apcu \
        igbinary \
        oauth \
        redis \
        uploadprogress \
    && pecl clear-cache \
    && apk del --purge $PHPIZE_DEPS \
    && rm -rf /var/cache/apk/* /usr/share/doc /usr/share/man /tmp/*

# Install PHIVE and some PHP tools.
RUN curl -sSL https://phar.io/releases/phive.phar -o phive.phar \
    && curl -sSL https://phar.io/releases/phive.phar.asc -o phive.phar.asc \
    && gpg --keyserver hkps://keys.openpgp.org --recv-keys 0x9D8A98B29B2D5D79 \
    && gpg --verify phive.phar.asc phive.phar \
    && chmod +x phive.phar \
    && mv phive.phar /usr/local/bin/phive \
    && rm phive.phar.asc \
    # Install latest version of Composer Normalize. Can not be installed with
    # project sources because of conflict with drupal-paranoia.
    && phive --no-progress install --global --trust-gpg-keys C00543248C87FB13 composer-normalize

# Install Pa11y-CI.
RUN apk update \
    && apk upgrade \
    && apk add \
        chromium \
        freetype \
        harfbuzz \
        nss \
        ttf-freefont \
    && rm -rf /var/cache/apk/*

RUN yarn global add \
        pa11y-ci@${PA11Y_CI_VERSION}.* \
        depcheck \
        --unsafe-perm \
    && yarn cache clean
